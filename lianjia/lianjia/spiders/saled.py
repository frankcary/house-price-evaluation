#!/usr/bin/env python
# Arthor: YouShaohua
# Date  : Jul.23 2017

# 抓取链家数据，分析二手房已销售情况
# scrapy crawl saled -o data/saled_20170618.json

import scrapy
import datetime
import json


class SaledSpider(scrapy.Spider):
    name = 'saled'

    start_urls = ['https://cq.lianjia.com/jingjiren/']

    # 获取各区的数据
    def parse(self, response):
        # follow links to author pages
        for href in response.css('.option-list a').xpath('@href').extract():
            if (href != '/jingjiren/'):
                yield response.follow(href, self.parse_area, meta={'firsturl': response.url})

    # 获取每个区下面的经纪人页数
    def parse_area(self, response):
        firsturl = response.meta.get('firsturl')
        dataPage = response.css('.house-lst-page-box::attr(page-data)').extract_first()
        totalPage = json.loads(dataPage)['totalPage'];

        for page in range(1, totalPage + 1):
            href = response.url + 'pg' + str(page) + '/'
            yield response.follow(href, self.parse_jing, meta={'firsturl': firsturl, 'secondurl': response.url});

    # 获取单个页面的经纪人
    def parse_jing(self, response):
        firsturl = response.meta.get('firsturl')
        secondurl = response.meta.get('secondurl')
        for ucid in response.css('.lianjiaim-createtalkAll::attr(data-ucid)').extract():
            tempHref = 'https://dianpu.lianjia.com/shop/ershoufang/' + ucid
            yield response.follow(url=tempHref, callback=self.parse_shop, meta={'ucid': ucid, 'firsturl': firsturl, 'secondurl': secondurl, 'thirdurl': response.url})

    # 获取每个经纪人的销售的房源
    def parse_shop(self, response):
        firsturl = response.meta.get('firsturl')
        secondurl = response.meta.get('secondurl')
        thirdurl = response.meta.get('thirdurl')
        ucid = response.meta.get('ucid')

        jsondata= json.loads(response.body_as_unicode())
        totalPage = jsondata['tplData']['sold_house']['pages']
        for page in range(1, totalPage + 1):
            href = response.url + '?p=' + str(page)+ '&type=1'
            yield response.follow(href, self.parse_end, meta={'firsturl': firsturl, 'secondurl': secondurl, 'thirdurl': thirdurl, 'foururl': response.url})

    def parse_end(self, response):
        firsturl = response.meta.get('firsturl')
        secondurl = response.meta.get('secondurl')
        thirdurl = response.meta.get('thirdurl')
        foururl = response.meta.get('foururl')
        yield {
            '1': firsturl,
            '2': secondurl,
            '3': thirdurl,
            '4': foururl,
            '5': response.url,
            'data': json.loads(response.body_as_unicode())
        }


