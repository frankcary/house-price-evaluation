#!/usr/bin/env python
# spider lianjia data 分析二手房销售增长数量
# scrapy crawl statiscs -o data/statiscs.json -t jsonlines
import scrapy
import datetime


# item class included here
class Myitem(scrapy.Item):
    # define the fields for your item here like:
    name = scrapy.Field()
    href = scrapy.Field()
    total = scrapy.Field()
    time = scrapy.Field()

class StatiscsSpider(scrapy.Spider):
    name = 'statiscs'

    start_urls = ['https://cq.lianjia.com/ershoufang/']

    def parse(self, response):
        # follow links to author pages
        for href in response.css('.position div[data-role=ershoufang] a::attr(href)'):
            yield response.follow(href, self.parse_total)

    def parse_total(self, response):
        now = datetime.datetime.now()
        def extract_with_css(query):
            return response.css(query).extract_first().strip()

        item = Myitem()
        item["href"] = response.url
        item['name'] = extract_with_css('.m-filter .position div[data-role=ershoufang] a.selected::text')
        item['total'] = extract_with_css('.leftContent .total span::text')
        item['time'] = now.strftime('%Y-%m-%d %H:%M')
        return item

