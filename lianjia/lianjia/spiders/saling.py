#!/usr/bin/env python
# Arthor: YouShaohua
# Date  : Sep.19 2017

# 此文件用户抓取链家重庆二手房在售房子信息
# 抓取列表: https://cq.lianjia.com/ershoufang/
# scrapy crawl saling -o data/saling.json

import scrapy
import re


# item class included here
class Myitem(scrapy.Item):
    url = scrapy.Field()
    buildYear = scrapy.Field()
    decoration = scrapy.Field()
    districtName = scrapy.Field()
    regionName = scrapy.Field()
    bedroomNum = scrapy.Field()
    square = scrapy.Field()
    price = scrapy.Field()

class SalingSpider(scrapy.Spider):
    name = 'saling'

    start_urls = ['https://cq.lianjia.com/ershoufang/pg2/']

    def parse(self, response):
        for href in response.css('.sellListContent > li .img').xpath('@href').extract():
            yield response.follow(href, self.parse_detail)

    def parse_detail(self, response):
        def extract_with_css(query):
            return response.css(query).extract_first().strip()


        #re.findall(r"\d+\.?\d*",string)

        item = Myitem()
        item['url'] = response.url
        item['buildYear'] = re.findall(r"\d+\.?\d*",extract_with_css('.houseInfo .area .subInfo::text'))[0]
        item['decoration'] = extract_with_css('.houseInfo .type .subInfo::text')
        item['districtName'] = extract_with_css('.aroundInfo .info a::text')
        #item['regionName'] = extract_with_css('.aroundInfo .info a::last::text')
        item['regionName'] = response.css('.aroundInfo .info a::text').extract()[1]
        item['bedroomNum'] = re.findall(r"\d+\.?\d*",extract_with_css('.houseInfo .room .mainInfo::text'))[0]
        item['square'] = re.findall(r"\d+\.?\d*",extract_with_css('.houseInfo .area .mainInfo::text'))[0]
        item['price'] = extract_with_css('.overview .price  .total::text')
        yield item
