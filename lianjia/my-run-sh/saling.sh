#!/usr/bin/env sh

# 自动抓取链家重庆在销售房价
DATE=`date +%Y%m%d`

# 数据文件
dataFile="./data/saling/data/saling_$DATE.json"

# 清除之前数据
rm ${dataFile}

# 数据抓取
scrapy crawl saling -o ${dataFile} 

# 将结果拷贝到训练目录
cp ${dataFile} ../_1_origin-data/saling.json
