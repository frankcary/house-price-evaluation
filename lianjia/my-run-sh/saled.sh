#!/usr/bin/env sh

DATE=`date +%Y%m%d`

# 数据文件
dataFile="./data/saled/data/saled_$DATE.json"

# 清除之前数据
rm ${dataFile}

# 数据抓取
scrapy crawl saled -o ${dataFile} 

# 将结果拷贝到训练目录
cp ${dataFile} ../_1_origin-data/saled.json

