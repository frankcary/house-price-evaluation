# What's The Project
It's a project using predict house price with machine learning.  
 I develop it in Aug 2017 when I know something about ML.

# Environment
python 3.5
tensorflow 1.x
scrapy 1.4

# How Do I implement?
First, Spider.    
Second, feature selection.  
Third, data handle and transform.  
Forth, train and test the model.
Fifth, predict.  
Know more by [PPT](https://prezi.com/of-i4uunesut/presentation/?utm_campaign=share&token=6e12420ecab4a6920eb5198e433594941b3c84e421bd464597e55a55d43a3f47&utm_medium=copy)

# Usage
spider saled data  
``
./lianjia/my-run-sh/saled.sh
``  
spider saling data  
``
./lianjia/my-run-sh/saling.sh
``  
exectue``./__main__.sh`` to handle data, train and test model and predict.


